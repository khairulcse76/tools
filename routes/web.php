<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//-------------Socialte Routes----------------
//-------------Socialte Routes----------------
//-------------Socialte Routes----------------
Route::get('login/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('login/facebook/callback/', 'Auth\LoginController@handleProviderCallback');

//-------------Ajax project Crud ----------------
Route::get('index-ajax', 'AjaxController@index');
Route::post('store-ajax', 'AjaxController@store');
Route::post('delete-ajax', 'AjaxController@delete');
Route::post('update-ajax', 'AjaxController@update');
Route::get('search-ajax', 'AjaxController@search');

//-------------Ajax project Crud ----------------
Route::get('toggleform', 'ToolsToogle@index');
Route::get('visitor', 'ToolsToogle@visitor');