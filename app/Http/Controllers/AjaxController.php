<?php

namespace App\Http\Controllers;

use App\Item;
use Illuminate\Http\Request;

class AjaxController extends Controller
{
    public function index(){
        $items=Item::orderBy('id', 'desc')->get();

        return view('ajax.list', compact('items'));
    }
    public function store(Request $request){
        $item=new Item;
        $item->item=$request->text;
        $item->save();
        return 'Data insert successful';
    }
    public function delete(Request $request){
        Item::where('id', $request->id)->delete();
        return $request->all();
    }
    public function update(Request $request){
       $item= Item::find($request->id);
        $item->item=$request->value;
        $item->save();
        return 'Item Successfully Update';
    }
    public function search(Request $request){

        $term=$request->term;
        $items=Item::where('item', 'like', '%'.$term.'%')->get();
        if (count($items)== 0){
            $SearchResult[]= 'Search Not Found';
        }else{
            foreach ($items as $value){
                $SearchResult[]=$value->item;
            }
        }
        return $SearchResult;
    }
}
