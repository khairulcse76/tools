<!DOCTYPE html>
<html>
<head>
    <title>Ajax Index</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
</head>
<body>

<div class="container">
    <br>
    <br>
    <div class="row">
        <div class="col-lg-offset-3 col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">To do Ajax list
                        <a href="" id="addNew" class="pull-right" data-toggle="modal" data-target="#popup"><i class="fa fa-plus" aria-hidden="true" ></i></a>
                    </h3>
                </div>
                <div class="panel-body" id="items">
                    <ul class="list-group">
                        <input title="text" name="search" id="searchItem" class="form-control" placeholder="Search">
                        @forelse($items as $item)
                        <li class="list-group-item ourItem" data-toggle="modal" data-target="#popup">{{ $item->item }}
                            <input type="hidden" name="id" id="itemId" value="{{ $item->id }}">
                        </li>
                        @empty
                            <li class="list-group-item ourItem" data-toggle="modal" data-target="#popup">List Empty</li>
                    @endforelse
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>


{{ csrf_field() }}
<div class="modal fade" id="popup" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="title">Add New Item</h4>
            </div>
            <div class="modal-body">
                <p>
                    <input type="text" id="addItem" class="form-control" placeholder="Enter List Here">
                    <input type="hidden" id="id" name="id">
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" id="close" data-dismiss="modal" style="display: none;">Close</button>
                <button type="button" class="btn btn-primary" id="editbutton" data-dismiss="modal" style="display: none;">Save changes</button>
                <button type="button" class="btn btn-primary" id="addButton" data-dismiss="modal">Add Item</button>
                <button type="button" class="btn btn-warning" id="deleteButton" data-dismiss="modal" style="display: none;">Delete</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="dialog" title="Amar Dialog Box">
    <p>This is the default dialog which is useful for displaying information. The dialog window can be moved, resized and closed with the 'x' icon.</p>
</div>

<script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script>
    $(document).ready(function () {
        $(document).on('click', '.ourItem',function (event) {
                var text=$(this).text();
                var id=$(this).find('#itemId').val();
                var text=$.trim(text);
                $('#addItem').val(text);
                $('#title').text('Edit Text');
                $('#deleteButton').show(400);
                $('#editbutton').show(400);
                $('#close').show(400);
                $('#addButton').hide(400);
                $('#id').val(id);
                console.log(id);
            });

        $('#addNew').click(function (event) {
            $('#addItem').val("");
            $('#title').text('Add New Item');
            $('#close').show(400);
            $('#addButton').show(400);
            $('#deleteButton').hide(400);
            $('#editbutton').hide(400);
        });

        $('#addButton').click(function (event) {
            var text=$('#addItem').val();
            if(text==''){
                alert('Item Field is required');
            }else {
                $.post('store-ajax', { 'text': text, '_token':$('input[name=_token]').val()}, function (data) {
                    alert(data);
                    $('#items').load(location.href + ' #items');
                });
            }
        });
        $('#deleteButton').click(function (event) {
            var id=$('#id').val();
            $.post('delete-ajax', { 'id': id, '_token':$('input[name=_token]').val()}, function (data) {
                $('#items').load(location.href + ' #items');
                console.log(data);
            });
        });
        $('#editbutton').click(function (event) {
            var id=$('#id').val();
            var value=$('#addItem').val();
            $.post('update-ajax', { 'id': id,'value': value, '_token':$('input[name=_token]').val()}, function (data) {
                alert(data);
                $('#items').load(location.href + ' #items');
            });
        });

        $( function() {
            $( "#searchItem" ).autocomplete({
                source: 'http://127.0.0.1:8000/search-ajax',
            });
        } );
        $( function() {
            $( "#dialog" ).dialog();
        } );
    });
</script>
</body>
</html>