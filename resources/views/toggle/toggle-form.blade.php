<!DOCTYPE html>
<html>
<head>
    <title>Ajax Index</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous"><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css" />
    <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
</head>
<body>

<div class="container">

<div class="row"><br>
    <div class="col-lg-offset-3 col-lg-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Form Validate in java Script</h3>
            </div>
            <div class="panel-body">
                <form action="{{ url('index-ajax') }}" id="the-form">
                <div class="form-group">
                    <label for="email" class="control-label">E-mail:</label>
                    <input name="email" class="form-control" id="email" placeholder="Enter email" data-validation="email">
                </div>

                    <div class="form-group">
                        <label for="email">User name</label>
                        <input type="text" name="user" class="form-control"  data-validation="length alphanumeric"
                               data-validation-length="3-12"
                               data-validation-error-msg="User name has to be an alphanumeric value (3-12 chars)">
                    </div>
                    <div class="form-group">
                        <label for="email">Password</label>
                        <input name="pass_confirmation" data-validation="strength" class="form-control" data-validation-strength="2">
                    </div>
                    <div class="form-group">
                        <label for="email">Repeat password</label>
                        <input name="pass" class="form-control" placeholder="Repeat your Password" data-validation="confirmation">
                    </div>
                    <div class="form-group">
                        <label for="dofbarth">Birth date</label>
                        <input name="birth" data-validation="birthdate" placeholder="date of birth" class="form-control"
                               data-validation-help="yyyy-mm-dd">
                    </div>
                    <div class="form-group">
                        <label for="country">Country</label>
                        <input name="country" id="country" data-validation="country" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="sepa">sepa</label>
                        <input type="text" data-validation="sepa" class="form-control">
                    </div>


                    <div class="form-group">
                        <label for="country">User Presentation (<span id="pres-max-length">100</span> characters left)</label>
                        <textarea name="presentation" id="presentation" class="form-control"></textarea>
                    </div>


                    <div class="form-group">
                        <input type="checkbox" data-validation="required"
                               data-validation-error-msg="You have to agree to our terms">
                        I agree to the <a href="..." target="_blank">terms of service</a>
                    </div>

                    <div class="form-group">
                        <input type="submit"  class="btn btn-warning">
                        <input type="reset" value="Reset form" class="btn btn-default">
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <script>

        $.validate({

            modules : 'location, date, security, file, sepa,',
            onModulesLoaded : function() {
                $('#country').suggestCountry();
            }
        });
        $('#my-textarea').restrictLength( $('#max-length-element') )
        // Restrict presentation length
        $('#presentation').restrictLength( $('#pres-max-length') );

    </script>
</div>
</body>
</html>